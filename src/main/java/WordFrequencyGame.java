import java.util.*;
import java.util.Arrays;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final int BASE_COUNT = 1;
    public static final String SPLIT_REGEX = "\\s+";
    public static final String DELIMITER = "\n";

    public String getResult(String inputStr) {
        try {
            List<Input> inputList = getTranfromToInputList(inputStr);
            inputList = getCountedInputList(inputList);
            inputList.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());
            return getCountedResultString(inputList);
        } catch (Exception exception) {
            throw new CalculateErrorException();
        }
    }

    private static String getCountedResultString(List<Input> inputList) {
        StringJoiner wordCountResultJoiner = new StringJoiner(DELIMITER);
        inputList.forEach(word ->
                wordCountResultJoiner.add(word.getValue() + " " + word.getWordCount()));
        return wordCountResultJoiner.toString();
    }

    private List<Input> getCountedInputList(List<Input> inputList) {
        //get the map for the next step of sizing the same word
        Map<String, Integer> wordsMap = getListMap(inputList);
        return wordsMap.entrySet().stream()
                .map(entry -> new Input(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    private static List<Input> getTranfromToInputList(String inputStr) {
        //split the input string with 1 to n pieces of spaces
        String[] wordsArray = inputStr.split(SPLIT_REGEX);
        return Arrays.stream(wordsArray)
                .map(word -> new Input(word, BASE_COUNT))
                .collect(Collectors.toList());
    }

    private Map<String,Integer> getListMap(List<Input> inputList) {
        Map<String, Integer> wordsMap = new HashMap<>();
        inputList.forEach(input -> {
            if (!wordsMap.containsKey(input.getValue())){
                wordsMap.put(input.getValue(), BASE_COUNT);
            }
            else {
                wordsMap.put(input.getValue(),  wordsMap.get(input.getValue()) + 1);
            }
        });
        return wordsMap;
    }

}
