public class CalculateErrorException extends RuntimeException{
    public CalculateErrorException() {
        super("Calculate Error");
    }
}
