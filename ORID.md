O: 
    . Today's main content is to learn how to refactor code. When we encounter four situations: Rule of Three, New Features, Bug Fix, and Code Review, we need to refactor. When refactoring, we need to follow the following four steps:
        1. Test Protection. 
        2. Find Code Smell.
        3. Refactor Techniques.
        4. Baby Step.
    . The team showed the Observer pattern in the presentation, and I participated in the final code presentation.
R: I feel fulfilled and nervous.
I: My first presentation on stage made me a bit nervous, but I still received positive feedback. I have learned a lot from refactoring, and with the guidance of my teacher, I have gained a preliminary understanding of how to perform refactoring.
D: Remember the situations that require refactoring and how to perform it.